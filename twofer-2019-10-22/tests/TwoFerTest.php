<?php

use PHPUnit\Framework\TestCase;
use function App\twoFer;

class TwoFerTest extends TestCase
{
    /** @test */
    public function it_can_accept_a_name()
    {
        $this->assertEquals("One for Alice, one for me.", twoFer('Alice'));
    }

    /** @test */
    public function it_can_accept_no_name()
    {
        $this->assertEquals("One for you, one for me.", twoFer());
    }
}
