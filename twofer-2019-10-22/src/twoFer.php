<?php

declare(strict_types=1);

namespace App;

function twoFer($name = 'you') {
    return "One for {$name}, one for me.";
}
