# Two Fer

Write a function, named `twoFer()`, that accepts 0 or 1 parameter (string), and returns a sentence in the form of:
> One for X, and one for me.

Ex: twoFer("Alice")
> One for Alice, and one for me.

Ex: twoFer()
> One for you, and one for me.