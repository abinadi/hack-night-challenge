# Stronger

## Love vs Friendship

Given that each letter in the alphabet has a numeric value:
(a=1, b=2, ... z=26),

write a function that accepts two strings, compares their value, and outputs which string is "stronger" (bigger). It should be case insensitive.

L + o + v + e = 54
F + r + i + e + n + s + h + i + p = 108

```
stronger("love", "friendship") => "friendship"

stronger("a", "z") => "z"

stronger("z", "a") => "z"
```