<?php

use PHPUnit\Framework\TestCase;

use function App\calcValue;
use function App\stronger;

class StrongerTest extends TestCase
{
    /** @test */
    public function can_calculate_the_value_of_a_string()
    {
        $this->assertEquals(54, calcValue('love'));
        $this->assertEquals(54, calcValue('LoVe'));
    }

    /** @test */
    public function it_can_determine_which_is_stronger()
    {
        $this->assertEquals("friendship", stronger("love", "friendship"));
        $this->assertEquals("friendship", stronger("friendship", "love"));
    }
}
