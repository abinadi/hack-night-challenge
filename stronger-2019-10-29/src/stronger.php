<?php

declare(strict_types=1);

namespace App;

function stronger(string $first, string $second): string
{
    return calcValue($first) > calcValue($second)
        ? $first
        : $second;
}

function calcValue(string $word): int
{
    $value = 0;

    for ($i = 0; $i < strlen($word); $i++) {
        $value += ord(strtolower($word[$i])) - 96;
    }

    return $value;
}
