<?php

declare(strict_types=1);

namespace App;

class Dice
{
    /** @var Rollable */
    protected $die1;

    /** @var Rollable */
    protected $die2;

    public function __construct(Rollable $die1, Rollable $die2)
    {
        $this->die1 = $die1;
        $this->die2 = $die2;
    }

    public function roll()
    {
        return $this->die1->roll() + $this->die2->roll();
    }
}