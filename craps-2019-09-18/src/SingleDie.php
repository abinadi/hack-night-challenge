<?php

declare(strict_types=1);

namespace App;

class SingleDie implements Rollable
{
    /** @var int */
    private $d;

    public function roll(): int
    {
        $this->d = rand(1, 6);
        return $this->d;
    }
}
