<?php

namespace Tests;

use App\Rollable;

class FakeDie implements Rollable
{
    /** @var int */
    private $toReturn;

    public function __construct(int $int)
    {
        $this->toReturn = $int;
    }

    public function roll(): int
    {
        return $this->toReturn;
    }
}
