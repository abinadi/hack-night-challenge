<?php

namespace App;

class Point
{
    /** @var int */
    public $x;

    /** @var int */
    public $y;

    /** @var int */
    public $steps;

    /** @var int */
    public $manhattan = 0;

    /** @var bool */
    public $twiceTouched;

    /** @var string */
    public $name;

    public function __construct(string $name, int $x, int $y, int $steps = 1)
    {
        $this->name = $name;
        $this->x = $x;
        $this->y = $y;
        $this->steps = $steps;
        $this->twiceTouched = false;
        $this->manhattan = $this->manhattan();
    }

    public function manhattan(int $x = 0, int $y = 0): int
    {
        $horizontal = abs($x - $this->x);
        $vertical = abs($y - $this->y);

        return $horizontal + $vertical;
    }
}
