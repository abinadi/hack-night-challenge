<?php

namespace App;

function follow(string $path1, $path2): array
{
    $instructions1 = explode(',', $path1);
    $instructions2 = explode(',', $path2);

    $grid = [];
    parseInstructions($instructions1, $grid, 'one');
    parseInstructions($instructions2, $grid, 'two');
    return $grid;
}

function parseInstructions(array $instructions, array &$grid, $name)
{
    $x = $y = $z = 0;
    foreach ($instructions as $line) {
        $direction = substr($line, 0, 1);
        $amount = intval(substr($line, 1));
        walk($amount, $direction, $y, $x, $z, $grid, $name);
    }
}

function walk(int $amount, string $direction, int &$x, int &$y, int &$z, array &$grid, $name)
{
    for ($i = 0; $i < $amount; $i++) {
        switch ($direction) {
            case 'U': $y++; break;
            case 'R': $x++; break;
            case 'D': $y--; break;
            case 'L': $x--; break;
        }
        $z++;
        if (isset($grid[$x][$y])) {
            if (!$grid[$x][$y]->twiceTouched && $name != $grid[$x][$y]->name) {
                $grid[$x][$y]->steps += $z;
                $grid[$x][$y]->twiceTouched = true;
            }
        } else {
            $grid[$x][$y] = new Point($name, $x, $y, $z);
        }
    }
}

function closestPoint(array $grid): Point
{
    $lowest = null;

    foreach ($grid as $outer) {
        foreach ($outer as $inner) {
            if ($inner->twiceTouched) {
                if ($lowest === null) {
                    $lowest = $inner;
                } elseif ($lowest->steps > $inner->steps) {
                    $lowest = $inner;
                }
            }
        }
    }

    return $lowest;
}
