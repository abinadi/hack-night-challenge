<?php

declare(strict_types=1);

use App\Point;
use PHPUnit\Framework\TestCase;

class PointTest extends TestCase
{
    /** @test */
    public function it_accepts_two_integers()
    {
        $this->expectException(ArgumentCountError::class);
        new Point();
    }

    /** @test */
    public function it_can_calculate_manhattan_distance()
    {
        $point = new Point('point', 3, 3);
        $this->assertEquals(6, $point->manhattan);
    }

    /** @test */
    public function it_can_tell_distance_between_negative_and_positive_point()
    {
        $negative = new Point('n',-2, -2);
        $positive = new Point('n',2, 2);

        $this->assertEquals(4, $negative->manhattan);
        $this->assertEquals(4, $positive->manhattan);
        $this->assertEquals(8, $negative->manhattan($positive->x, $positive->y));
    }

    /** @test */
    public function it_can_tell_distance_between_two_negative_points()
    {
        $one = new Point('n',-4, -4);
        $two = new Point('n',-3, -3);

        $this->assertEquals(8, $one->manhattan);
        $this->assertEquals(6, $two->manhattan);
        $this->assertEquals(2, $one->manhattan($two->x, $two->y));
    }

    /** @test */
    public function it_only_accepts_two_integers()
    {
        $this->assertInstanceOf(Point::class, new Point('n',0, 0));
    }
}
