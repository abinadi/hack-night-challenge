<?php

declare(strict_types=1);

use App\Point;
use function App\follow;
use function App\closestPoint;
use PHPUnit\Framework\TestCase;

class Advent03Part1Test extends TestCase
{
    /** @test */
    public function it_can_follow_two_paths()
    {
        $wire1 = 'R5,U3,L8,D6';
        $wire2 = 'U2,L4,D3,R3,L4';

        $result = follow($wire1, $wire2);

        $this->assertIsArray($result);
        $this->assertEquals(22, $result[-3][-3]->steps);
        $this->assertEquals(12, $result[-1][-1]->steps);
        $this->assertEquals(22, $result[-3][2]->steps);
        $this->assertEquals(30, $result[-3][-1]->steps);
        $this->assertEquals(5, $result[-3][2]->manhattan);
        $this->assertEquals(4, $result[-3][-1]->manhattan);
    }

    /** @test */
    public function it_can_find_the_closest_steps()
    {
        $wire1 = 'R75,D30,R83,U83,L12,D49,R71,U7,L72';
        $wire2 = 'U62,R66,U55,R34,D71,R55,D58,R83';

        $result = follow($wire1, $wire2);
        $closestPoint = closestPoint($result);

        $this->assertEquals(610, $closestPoint->steps);
    }

    /** @test */
    public function it_can_find_the_closest_steps2()
    {
        $wire1 = 'R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51';
        $wire2 = 'U98,R91,D20,R16,D67,R40,U7,R15,U6,R7';

        $result = follow($wire1, $wire2);
        $closestPoint = closestPoint($result);

        $this->assertEquals(410, $closestPoint->steps);
    }

    /** @test */
    public function it_can_solve_the_problem()
    {
        $code = file(__DIR__ . '/../src/input.txt');
        $result = follow($code[0], $code[1]);
        $closestPoint = closestPoint($result);

        $this->assertEquals(35194, $closestPoint->steps);
    }
}
