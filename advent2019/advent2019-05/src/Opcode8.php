<?php

declare(strict_types=1);

namespace App;

class Opcode8 extends Opcode implements OpcodeInterface
{
    public static function operate(array &$code, Parameters $parameters, int $index, int $input = null): void
    {
        $first = self::getPlace($code, $parameters, $index, 1, 'first');
        $second = self::getPlace($code, $parameters, $index, 2, 'second');
        if ($first == $second) {
            $code[$code[$index + 3]] = 1;
        } else {
            $code[$code[$index + 3]] = 0;
        }
    }

    public static function increment(int $index, int $count): int
    {
        return $index + 4;
    }
}
