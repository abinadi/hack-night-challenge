<?php

declare(strict_types=1);

namespace App;

function test(string $code, int $input): string
{
    return implode(',', parse(explode(',', $code), $input));
}

function intcode(string $code): string
{
    return implode(',', parse(explode(',', $code)));
}

function parse(array $code, ?int $input = null): array
{
    $count = count($code);

    $i = 0;
    while ($i < $count) {
        $parameters = breakdown($code[$i]);
        /** @var OpcodeInterface $opcode */
        $opcode = "\\App\\Opcode{$parameters->opcode}";
        $opcode::operate($code, $parameters, $i, $input);
        $i = $opcode::increment($i, $count);
    }

    return $code;
}

function breakdown($code): Parameters
{
    $code = (string)$code;
    $opcode = $code;
    $first = $second = $third = 0;

    if (strlen($code) > 2) {
        $opcode = intval(substr($code, -2));
        if (strlen($code) >= 3) {
            $first = intval(substr($code, -3, 1));
        }
        if (strlen($code) >= 4) {
            $second = intval(substr($code, -4, 1));
        }
        if (strlen($code) >= 5) {
            $third = intval(substr($code, -5, 1));
        }
    }

    return new Parameters((int)$opcode, $first, $second, $third);
}
