<?php

declare(strict_types=1);

namespace App;

class Opcode4 extends Opcode implements OpcodeInterface
{
    public static function operate(array &$code, Parameters $parameters, int $index, int $input = null): void
    {
        if ($parameters->first == 0) {
            echo $code[$code[$index + 1]];
        } else {
            echo $code[$index + 1];
        }
    }

    public static function increment(int $index, int $count): int
    {
        return $index + 2;
    }
}
