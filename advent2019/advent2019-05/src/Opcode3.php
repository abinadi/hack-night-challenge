<?php

declare(strict_types=1);

namespace App;

class Opcode3 extends Opcode implements OpcodeInterface
{
    public static function operate(array &$code, Parameters $parameters, int $index, int $input = null): void
    {
        if ($parameters->first == 0) {
            $code[$code[$index + 1]] = $input;
        } else {
            $code[$index + 1] = $input;
        }
    }

    public static function increment(int $index, int $count): int
    {
        return $index + 2;
    }
}
