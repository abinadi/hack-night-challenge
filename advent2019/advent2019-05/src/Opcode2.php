<?php

declare(strict_types=1);

namespace App;

class Opcode2 extends Opcode implements OpcodeInterface
{
    public static function operate(array &$code, Parameters $parameters, int $index, int $input = null): void
    {
        $first = self::getPlace($code, $parameters, $index, 1, 'first');
        $second = self::getPlace($code, $parameters, $index, 2, 'second');
        $code[$code[$index + 3]] = $first * $second;
    }

    public static function increment(int $index, int $count): int
    {
        return $index + 4;
    }
}
