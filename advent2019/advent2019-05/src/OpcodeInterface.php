<?php

declare(strict_types=1);

namespace App;

interface OpcodeInterface
{
    public static function getPlace(array $code, Parameters $parameters, int $i, int $amount, string $p): int;

    public static function operate(array &$code, Parameters $parameters, int $index, int $input = null): void;

    public static function increment(int $index, int $count): int;
}
