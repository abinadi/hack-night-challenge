<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use function App\intcodeComputer;

class UpperCodes5678Test extends TestCase
{
    /** @test */
    public function it_will_output_999_if_input_is_under_8()
    {
        $this->expectOutputString('999');
        $program = '3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99';
        intcodeComputer($program, 5);
    }

    /** @test */
    public function it_will_output_1000_if_input_is_8()
    {
        $this->expectOutputString('1000');
        $program = '3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99';
        intcodeComputer($program, 8);
    }

    /** @test */
    public function it_will_output_1001_if_input_is_over_8()
    {
        $this->expectOutputString('1001');
        $program = '3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99';
        intcodeComputer($program, 9);
    }

    /** @test */
    public function it_can_find_the_correct_output_for_the_provided_input()
    {
        $this->expectOutputString('652726');

        $file = __DIR__ . '/../src/input.txt';
        $code = file_get_contents($file);
        intcodeComputer($code, 5);
    }
}
