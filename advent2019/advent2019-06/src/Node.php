<?php

declare(strict_types=1);

namespace App;

class Node
{
    /** @var string */
    public $name;

    /** @var array */
    public $children = [];

    /** @var string */
    public $parent;

    /**
     * @param string $node
     * @throws NodeFormatException
     */
    public function __construct(string $node = null)
    {
        if ($node !== null) {
            $this->splitAndSet($node);
        }
    }

    /**
     * @param string $node
     * @throws NodeFormatException
     */
    protected function splitAndSet(string $node): void
    {
        $parts = explode(')', $node);

        if (count($parts) !== 2) {
            throw new NodeFormatException('Node must be in the format A)B');
        }

        $this->name = trim($parts[0]);
        $this->children[] = trim($parts[1]);
    }
}
