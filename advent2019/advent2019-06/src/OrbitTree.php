<?php

declare(strict_types=1);

namespace App;

use SplDoublyLinkedList;

class OrbitTree
{
    /** @var SplDoublyLinkedList */
    private $map;

    /**
     * @param array $map
     * @throws NodeFormatException
     */
    public function __construct(array $map = [])
    {
        $this->createTree($map);
    }

    public function size(): int
    {
        return $this->map->count();
    }

    public function totalOrbits(string $nodeName): int
    {
        $total = 0;
        $node = $this->find($nodeName);

        while (!$this->isRoot($node)) {
            $node = $this->find($node->parent);
            $total++;
        }

        return $total;
    }

    public function totalMapOrbits(): int
    {
        $total = 0;
        $this->map->rewind();

        while ($this->map->valid()) {
            $key = $this->map->key();
            $total += $this->totalOrbits($this->map->current()->name);
            $this->setIterator($key); // set the iterator to the correct pointer
            $this->map->next();
        }

        return $total;
    }

    public function root(): ?Node
    {
        $this->map->rewind();

        while ($this->map->valid()) {
            if ($this->map->current()->parent === null) {
                return $this->map->current();
            }
            $this->map->next();
        }

        return null;
    }

    public function orbitalTransfers(): int
    {
        // Get orbit path to root of both nodes (YOU and SAN)
        $you = $this->orbitPath('YOU');
        $san = $this->orbitPath('SAN');

        // Count the two paths, eliminating the common nodes
        $youdiff = array_diff($you, $san);
        $sandiff = array_diff($san, $you);

        return count($youdiff) + count($sandiff);
    }

    private function orbitPath(string $nodeName): array
    {
        $node = $this->find($nodeName);

        while (!$this->isRoot($node)) {
            $node = $this->find($node->parent);
            $path[] = $node->name;
        }

        return $path;
    }

    private function setIterator(int $key): void
    {
        while ($this->map->key() != $key) {
            ($this->map->key() > $key)
                ? $this->map->prev()
                : $this->map->next();
        }
    }

    /**
     * @param array|null $map
     * @throws NodeFormatException
     */
    private function createTree(array $map = []): void
    {
        $this->map = new SplDoublyLinkedList();

        while(!empty($map)) {
            $node = new Node(array_shift($map));

            $existingNode = $this->find($node->name);
            $this->createOrUpdateNode($existingNode, $node);
        }
    }

    private function isRoot(Node $node)
    {
        return $node->parent === null;
    }

    private function createOrUpdateNode(?Node $existingNode, Node $node): void
    {
        if ($existingNode !== null) {
            $this->map->current()->children[] = $node->children[0];
        } else {
            $this->map->push($node);
        }
        $this->createOrSetOrbiterWithParent($node);
    }

    private function createOrSetOrbiterWithParent(Node $node): void
    {
        $found = $this->find($node->children[0]);
        if ($found === null) {
            $this->map->push($this->createOrbiter($node));
        } else {
            $this->map->current()->parent = $node->name;
        }
    }

    private function find(string $name): ?Node
    {
        $this->map->rewind();
        while ($this->map->valid()) {
            if ($this->map->current()->name == $name) {
                return $this->map->current();
            }
            $this->map->next();
        }

        return null;
    }

    private function createOrbiter(Node $node): Node
    {
        $orbiter = new Node();
        $orbiter->name = $node->children[0];
        $orbiter->parent = $node->name;

        return $orbiter;
    }
}
