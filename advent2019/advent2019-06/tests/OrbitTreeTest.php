<?php

declare(strict_types=1);

use App\NodeFormatException;
use App\OrbitTree;
use PHPUnit\Framework\TestCase;

class OrbitTreeTest extends TestCase
{
    /** @test */
    public function can create an orbit tree()
    {
        $orbitTree = new OrbitTree();
        $this->assertInstanceOf(OrbitTree::class, $orbitTree);
    }

    /**
     * @test
     * @throws NodeFormatException
     */
    public function can add nodes via the constructor()
    {
        $orbitTree = new OrbitTree(['A)B','B)C','C)D','D)E']);
        $this->assertEquals(5, $orbitTree->size());
        $orbitTree = new OrbitTree(['A)B','B)C','C)D','B)E']);
        $this->assertEquals(5, $orbitTree->size());
    }
}
