<?php

declare(strict_types=1);

use App\NodeFormatException;
use App\OrbitTree;
use PHPUnit\Framework\TestCase;

class OrbitalTransfersTest extends TestCase
{
    /**
     * @test
     * @throws NodeFormatException
     */
    public function can calculate the minimum orbital transfers()
    {
        $orbitMap = "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L\nK)YOU\nI)SAN";
        $orbitMap = explode("\n", $orbitMap);
        $orbit = new OrbitTree($orbitMap);

        $this->assertEquals(4, $orbit->orbitalTransfers());
    }

    /**
     * @test
     * @throws NodeFormatException
     */
    public function it can find the solution to part two()
    {
        $map = file(__DIR__.'/../src/input.txt');
        $orbit = new OrbitTree($map);

        $this->assertEquals(259, $orbit->orbitalTransfers());
    }
}
