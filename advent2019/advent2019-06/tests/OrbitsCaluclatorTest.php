<?php

declare(strict_types=1);

use App\NodeFormatException;
use App\OrbitTree;
use PHPUnit\Framework\TestCase;

class OrbitsCaluclatorTest extends TestCase
{
    /**
     * @test
     * @throws NodeFormatException
     */
    public function it can take a map and count total orbits()
    {
        $map = "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L";
        $map = explode("\n", $map);
        $orbit = new OrbitTree($map);

        $this->assertEquals(12, $orbit->size());
        $this->assertEquals(3, $orbit->totalOrbits('D'));
        $this->assertEquals(7, $orbit->totalOrbits('L'));
        $this->assertEquals(0, $orbit->totalOrbits('COM'));
    }

    /**
     * @test
     * @throws NodeFormatException
     */
    public function it can find total orbits for entire map()
    {
        $map = "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L";
        $map = explode("\n", $map);
        $orbit = new OrbitTree($map);

        $this->assertEquals(42, $orbit->totalMapOrbits());
    }

    /**
     * @test
     * @throws NodeFormatException
     */
    public function it can find the solution to part one()
    {
        $this->markTestSkipped();
        $map = file(__DIR__.'/../src/input.txt');
        $orbit = new OrbitTree($map);

        $this->assertEquals(144909, $orbit->totalMapOrbits());
    }
}
