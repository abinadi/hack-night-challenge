<?php

declare(strict_types=1);

use App\Node;
use App\NodeFormatException;
use PHPUnit\Framework\TestCase;

class NodeTest extends TestCase
{
    /** @test */
    public function node string must have specific format()
    {
        $this->expectException(NodeFormatException::class);

        new Node('COM]B');
    }
}
