<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use function App\intcode;

class Advent02Part1Test extends TestCase
{
    /** @test */
    public function it_halts_at_99()
    {
        $this->assertEquals('99', intcode('99'));
        $this->assertEquals('99,0,0,0,1,0,0,0,99', intcode('99,0,0,0,1,0,0,0,99'));
    }

    /** @test */
    public function it_can_handle_simple_intcodes()
    {
        $this->assertEquals('2,0,0,0,99', intcode('1,0,0,0,99'));
        $this->assertEquals('2,3,0,6,99', intcode('2,3,0,3,99'));
        $this->assertEquals('2,4,4,5,99,9801', intcode('2,4,4,5,99,0'));
        $this->assertEquals('30,1,1,4,2,5,6,0,99', intcode('1,1,1,4,99,5,6,0,99'));
        $this->assertEquals('3500,9,10,70,2,3,11,0,99,30,40,50', intcode('1,9,10,3,2,3,11,0,99,30,40,50'));
    }

    /** @test */
    public function it_can_solve_the_problem()
    {
        $code = file_get_contents(__DIR__ . '/../src/input.txt');
        $code = explode(',', $code);
        $code[1] = 12;
        $code[2] = 2;
        $code = implode(',', $code);

        $result = intcode($code);
        $result = explode(',', $result);

        $this->assertEquals('4484226', $result[0]);
    }
}
