<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use function App\parse;

class Advent02Part2Test extends TestCase
{
    /** @test */
    public function it_can_solve_the_problem()
    {
        $code = file_get_contents(__DIR__ . '/../src/input.txt');
        $code = explode(',', $code);
        $out = 0;

        for ($i = 0; $i < 99; $i++) {
            for ($j = 0; $j < 99; $j++) {
                $code[1] = $i;
                $code[2] = $j;

                $result = parse($code);
                if ($result[0] == '19690720') {
                    $out = 100 * $i + $j;
                    break;
                }
            }
            if ($result[0] == '19690720') {
                break;
            }
        }
        $this->assertEquals('19690720', $result[0]);
        $this->assertEquals(5696, $out);
    }
}
