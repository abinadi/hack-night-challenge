<?php

declare(strict_types=1);

namespace App;

function intcode(string $code): string
{
    return implode(',', parse(explode(',', $code)));
}

function parse(array $code): array
{
    $count = count($code);

    for ($i = 0; $i < $count; $i += 4) {
        switch (intval($code[$i])) {
            case 1:
                $code[$code[$i + 3]] = $code[$code[$i + 1]] + $code[$code[$i + 2]];
                break;
            case 2:
                $code[$code[$i + 3]] = $code[$code[$i + 1]] * $code[$code[$i + 2]];
                break;
            case 99:
                $i = $count;
                break;
            default:
                break;
        }
    }

    return $code;
}
