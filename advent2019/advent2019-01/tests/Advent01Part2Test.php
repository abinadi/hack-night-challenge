<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use function App\fuelRequiredRecursive;

class Advent01Part2Test extends TestCase
{
    /** @test */
    public function it_can_stop_recurring_for_14()
    {
        $this->assertEquals(2, fuelRequiredRecursive(14));
    }

    /** @test */
    public function it_can_recur_for_1969()
    {
        $this->assertEquals(966, fuelRequiredRecursive(1969));
    }

    /** @test */
    public function it_can_recur_for_100756()
    {
        $this->assertEquals(50346, fuelRequiredRecursive(100756));
    }

    /** @test */
    public function it_can_sum_for_all_modules()
    {
        $file = __DIR__ . '/../src/input.txt';
        $masses = file($file);
        $total = 0;

        foreach($masses as $mass) {
            $total += fuelRequiredRecursive(intval($mass));
        }

        $this->assertEquals(5068210, $total);
    }
}
