<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use function App\fuelRequired;

class Advent01Test extends TestCase
{
    /** @test */
    public function it_can_calculate_fuel_requirement_for_mass_of_12()
    {
        $this->assertEquals(2, fuelRequired(12));
    }

    /** @test */
    public function it_can_calculate_for_mass_14()
    {
        $this->assertEquals(2, fuelRequired(14));
    }

    /** @test */
    public function it_can_calculate_for_mass_1969()
    {
        $this->assertEquals(654, fuelRequired(1969));
    }

    /** @test */
    public function it_can_calculate_for_mass_100756()
    {
        $this->assertEquals(33583, fuelRequired(100756));
    }

    /** @test */
    public function it_can_sum_for_all_modules()
    {
        $file = __DIR__ . '/../src/input.txt';
        $masses = file($file);
        $total = 0;

        foreach($masses as $mass) {
            $total += fuelRequired(intval($mass));
        }

        $this->assertEquals(3380731, $total);
    }
}
