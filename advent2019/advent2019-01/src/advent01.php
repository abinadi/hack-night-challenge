<?php

declare(strict_types=1);

namespace App;

function fuelRequired(int $mass): int
{
    return (int) floor($mass / 3) - 2;
}

function fuelRequiredRecursive(int $mass): int
{
    $fuelRequired = fuelRequired($mass);
    if ($fuelRequired <= 0) {
        return 0;
    }
    return $fuelRequired + fuelRequiredRecursive($fuelRequired);
}
