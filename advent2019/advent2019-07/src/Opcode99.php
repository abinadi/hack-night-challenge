<?php

declare(strict_types=1);

namespace App;

class Opcode99 extends Opcode implements OpcodeInterface
{
    public static function operate(array &$code, Parameters $parameters, int $index, int $input = null): void
    {
        // Do nothing since this opcode will end the program.
    }

    public static function increment(int $index, int $count): int
    {
        return $count;
    }
}
