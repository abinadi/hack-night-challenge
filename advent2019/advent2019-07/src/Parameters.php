<?php

declare(strict_types=1);

namespace App;

class Parameters
{
    public $opcode = 0;
    public $first = 0;
    public $second = 0;
    public $third = 0;

    public function __construct(int $opcode, int $first, int $second, int $third)
    {
        $this->opcode = $opcode;
        $this->first = $first;
        $this->second = $second;
        $this->third = $third;
    }
}
