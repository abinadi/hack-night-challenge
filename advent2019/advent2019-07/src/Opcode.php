<?php

declare(strict_types=1);

namespace App;

class Opcode
{
    public static function getPlace(array $code, Parameters $parameters, int $i, int $amount, string $p): int
    {
        if ($parameters->$p == 0) {
            $place = $code[$code[$i + $amount]];
        } else {
            $place = $code[$i + $amount];
        }
        return (int)$place;
    }
}
