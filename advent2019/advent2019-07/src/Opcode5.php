<?php

declare(strict_types=1);

namespace App;

class Opcode5 extends Opcode implements OpcodeInterface
{
    protected static $ipModified = false;
    private static $secondParameter;

    public static function operate(array &$code, Parameters $parameters, int $index, int $input = null): void
    {
        $first = self::getPlace($code, $parameters, $index, 1, 'first');
        $second = self::getPlace($code, $parameters, $index, 2, 'second');
        if ($first != 0) {
            $code[$index] = $second;
            self::$ipModified = true;
            self::$secondParameter = $second;
        }
    }

    public static function increment(int $index, int $count): int
    {
        if (self::$ipModified) {
            self::$ipModified = false;
            return self::$secondParameter;
        } else {
            return $index + 3;
        }
    }
}
