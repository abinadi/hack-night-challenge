<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use function App\intcode;
use function App\intcodeComputer;

class IntcodeBaseTest extends TestCase
{
    /** @test */
    public function it_halts_at_99()
    {
        $this->assertEquals('99', intcode('99'));
        $this->assertEquals('99,0,0,0,1,0,0,0,99', intcode('99,0,0,0,1,0,0,0,99'));
    }

    /** @test */
    public function it_can_handle_simple_intcodes()
    {
        $this->assertEquals('2,0,0,0,99', intcode('1,0,0,0,99'));
        $this->assertEquals('2,3,0,6,99', intcode('2,3,0,3,99'));
        $this->assertEquals('2,4,4,5,99,9801', intcode('2,4,4,5,99,0'));
        $this->assertEquals('30,1,1,4,2,5,6,0,99', intcode('1,1,1,4,99,5,6,0,99'));
        $this->assertEquals('3500,9,10,70,2,3,11,0,99,30,40,50', intcode('1,9,10,3,2,3,11,0,99,30,40,50'));
    }

    /** @test */
    public function it_can_handle_input_codes()
    {
        $this->assertEquals('4,0,99', intcodeComputer('3,0,99', [4]));
    }

    /** @test */
    public function it_can_handle_output_codes()
    {
        $this->expectOutputString('12');
        $this->assertEquals('4,3,99,12', intcode('4,3,99,12'));
    }

    /** @test */
    public function it_can_handle_input_output_codes()
    {
        $this->expectOutputString('100');
        $this->assertEquals('100,0,4,0,99', intcodeComputer('3,0,4,0,99', [100]));
    }

    /** @test */
    public function it_can_handle_parameter_modes()
    {
        $this->assertEquals('1002,4,3,4,99', intcode('1002,4,3,4,33'));
    }

    /** @test */
    public function it_can_find_the_correct_output_for_the_provided_input()
    {
        $this->expectOutputString('00000000015314507');

        $file = __DIR__ . '/../src/advent05-input.txt';
        $code = file_get_contents($file);
        intcodeComputer($code, [1]);
    }
}
