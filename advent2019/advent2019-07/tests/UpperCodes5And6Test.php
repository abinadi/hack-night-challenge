<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use function App\intcodeComputer;

class UpperCodes5And6Test extends TestCase
{
    /** @test */
    public function it_can_jump_for_zero_input_using_position_mode()
    {
        $this->expectOutputString('0');
        intcodeComputer('3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9', [0]);
    }

    /** @test */
    public function it_can_jump_for_nonzero_input_using_position_mode()
    {
        $this->expectOutputString('1');
        intcodeComputer('3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9', [8]);
    }

    /** @test */
    public function it_can_jump_for_zero_input_using_immediate_mode()
    {
        $this->expectOutputString('0');
        intcodeComputer('3,3,1105,-1,9,1101,0,0,12,4,12,99,1', [0]);
    }

    /** @test */
    public function it_can_jump_for_nonzero_input_using_immediate_mode()
    {
        $this->expectOutputString('1');
        intcodeComputer('3,3,1105,-1,9,1101,0,0,12,4,12,99,1', [5]);
    }
}
