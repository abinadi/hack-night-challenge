<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use function App\intcodeComputer;

class UpperOpcodes7And8Test extends TestCase
{
    /** @test */
    public function it_can_compare_8_to_8_using_position_mode()
    {
        $this->expectOutputString('1');
        intcodeComputer('3,9,8,9,10,9,4,9,99,-1,8', [8]);
    }

    /** @test */
    public function it_can_compare_9_to_8_using_position_mode()
    {
        $this->expectOutputString('0');
        intcodeComputer('3,9,8,9,10,9,4,9,99,-1,8', [9]);
    }

    /** @test */
    public function it_can_compare_8_to_8_using_immediate_mode()
    {
        $this->expectOutputString('1');
        intcodeComputer('3,3,1108,-1,8,3,4,3,99', [8]);
    }

    /** @test */
    public function it_can_compare_9_to_8_using_immediate_mode()
    {
        $this->expectOutputString('0');
        intcodeComputer('3,3,1108,-1,8,3,4,3,99', [9]);
    }

    /** @test */
    public function it_can_test_5_less_than_8_using_position_mode()
    {
        $this->expectOutputString('1');
        intcodeComputer('3,9,7,9,10,9,4,9,99,-1,8', [5]);
    }

    /** @test */
    public function it_can_test_9_less_than_8_using_position_mode()
    {
        $this->expectOutputString('0');
        intcodeComputer('3,9,7,9,10,9,4,9,99,-1,8', [9]);
    }

    /** @test */
    public function it_can_test_5_less_than_8_using_immediate_mode()
    {
        $this->expectOutputString('1');
        intcodeComputer('3,3,1107,-1,8,3,4,3,99', [5]);
    }

    /** @test */
    public function it_can_test_9_less_than_8_using_immediate_mode()
    {
        $this->expectOutputString('0');
        intcodeComputer('3,3,1107,-1,8,3,4,3,99', [9]);
    }
}
