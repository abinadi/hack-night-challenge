<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use function App\intcodeComputer;

class AmplificationCircuitTest extends TestCase
{
    /** @test */
    public function can take multiple inputs()
    {
        $inputs = [4];

        $this->assertEquals('4,0,99', intcodeComputer('3,0,99', $inputs));
    }
}
