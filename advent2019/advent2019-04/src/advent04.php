<?php

declare(strict_types=1);

namespace App;

function password(int $low, int $high): int
{
    if ($low < 100000) {
        throw new \Exception('Must be at least six digits.');
    }

    if ($low > $high) {
        throw new \Exception('First parameter must be lower than second.');
    }

    if ($high > 999999) {
        throw new \Exception('Must be no more than six digits');
    }

    $count = 0;
    for ($i = $low; $i <= $high; $i++) {
        if (onlyIncreases($i) && hasDoubles($i)) {
            $count++;
        }
    }

    return $count;
}

function onlyIncreases(int $number): bool
{
    $prev = null;
    while($number > 0) {
        $temp = $number % 10;
        if ($prev !== null && $prev < $temp) {
            return false;
        }
        $prev = $temp;
        $number = floor($number / 10);
    }
    return true;
}

function hasDoubles(int $number): bool
{
    // Remove any numbers repeated more than twice.
    $number = preg_replace('/(\d)\1{2,}/', '', $number);
    // If there are any doubles left, we have met the criterion!
    return preg_match('/(\d)\1{1,}/', $number, $out) === 1;
}
