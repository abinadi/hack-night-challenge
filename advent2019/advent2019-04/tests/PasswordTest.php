<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use function App\hasDoubles;
use function App\password;
use function App\onlyIncreases;

class PasswordTest extends TestCase
{
    /**
     * @test
     * @throws Exception
     */
    public function it_can_check_for_no_less_than_six_digits()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Must be at least six digits');
        password(1, 100000);
    }

    /**
     * @test
     * @throws Exception
     */
    public function it_must_go_from_low_to_high()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('First parameter must be lower than second.');
        password(100001, 100000);
    }

    /**
     * @test
     * @throws Exception
     */
    public function it_can_check_for_no_more_than_six_digits()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Must be no more than six digits');
        password(100000, 1000000);
    }

    /**
     * @test
     * @throws Exception
     */
    public function it_can_count_numbers_that_meet_criteria_within_a_range()
    {
        $this->assertEquals(2, password(112233, 112234));
        $this->assertEquals(1, password(112233, 112233));
        //$this->assertEquals(1660, password(172851, 675869));
        $this->assertEquals(1135, password(172851, 675869));
    }

    /** @test */
    public function it_can_only_increase()
    {
        $this->assertTrue(onlyIncreases(123456));
        $this->assertTrue(onlyIncreases(222222));
        $this->assertFalse(onlyIncreases(123056));
        $this->assertFalse(onlyIncreases(222221));
    }

    /** @test */
    public function it_contains_at_least_one_set_of_doubles()
    {
        $this->assertTrue(hasDoubles(112345));
        $this->assertTrue(hasDoubles(123445));
        $this->assertTrue(hasDoubles(112223));
        $this->assertTrue(hasDoubles(111122));

        $this->assertFalse(hasDoubles(111243));
        $this->assertFalse(hasDoubles(122243));
        $this->assertFalse(hasDoubles(123456));
        $this->assertFalse(hasDoubles(121212));
    }
}
