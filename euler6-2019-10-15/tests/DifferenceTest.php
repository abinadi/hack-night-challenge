<?php

use PHPUnit\Framework\TestCase;
use function App\sumOfSquares;
use function App\squareOfSum;
use function App\difference;

class DifferenceTest extends TestCase
{
    /** @test */
    public function can_get_sum_of_squares()
    {
        $this->assertEquals(385, sumOfSquares(1, 10));
        $this->assertEquals(338350, sumOfSquares(1, 100));
    }

    /** @test */
    public function can_get_square_of_sum()
    {
        $this->assertEquals(3025, squareOfSum(1, 10));
        $this->assertEquals(25502500, squareOfSum(1, 100));
    }

    /** @test */
    public function can_get_difference_between_the_two()
    {
        $this->assertEquals(-25164150, difference(1, 100));
    }
}
