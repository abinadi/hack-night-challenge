<?php

namespace App;

function sumOfSquares($begin, $end) {
    $numbers = range($begin, $end);
    $sum = 0;

    foreach($numbers as $number) {
        $sum += pow($number, 2);
    }

    return $sum;
}

function squareOfSum($begin, $end) {
    $sum = array_sum(range($begin, $end));
    return pow($sum, 2);
}

function difference($begin, $end) {
    return sumOfSquares($begin, $end) - squareOfSum($begin, $end);
}
