<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use function App\pangram;

class PangramTest extends TestCase
{
    /** @test */
    public function the_whole_alphabet_count()
    {
        $this->assertTrue(pangram('abcdefghijklmnopqrstuvwxyz'));
        $this->assertTrue(pangram('AbCdEfGhIjKlMnOpQrStUvWxYz'));
        $this->assertTrue(pangram('The quick brown fox jumps over the lazy dog.'));
    }

    public function not_using_the_whole_alphabet_fails()
    {
        $this->assertFalse(pangram('a'));
        $this->assertFalse(pangram('The quick brown fox jumped over the lazy dog.'));
    }
}
