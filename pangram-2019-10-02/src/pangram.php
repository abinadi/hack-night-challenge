<?php

declare(strict_types=1);

namespace App;

function pangram($phrase) {
    foreach(range('a', 'z') as $letter) {
        if(!stristr($phrase, $letter)) {
            return false;
        }
    }

    return true;
}
