<?php

declare(strict_types=1);

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use function App\isogram;

class IsogramTest extends TestCase
{
    /** @test */
    public function returns_true_on_single_letter_word()
    {
        $this->assertTrue(isogram('a'));
    }

    /** @test */
    public function returns_false_on_double_letter_word()
    {
        $this->assertFalse(isogram('aa'));
    }

    /**
     * @dataProvider nonIsograms
     * @test
     */
    public function does_not_work($word)
    {
        $this->assertFalse(isogram($word));
    }

    public function nonIsograms()
    {
        return [
            ['challenge'],
            ['Abinadi'],
            ['isograms'],
            ['starting']
        ];
    }

    /**
     * @dataProvider isograms
     * @test
     */
    public function works($word)
    {
        $this->assertTrue(isogram($word));
    }

    public function isograms()
    {
        return [
            ['lumberjacks'],
            ['background'],
            ['downstream'],
            ['six-year-old'],
            ['six year old']
        ];
    }
}
