<?php

declare(strict_types=1);

namespace App;

function isogram(string $word): bool
{
    $word = strtolower(trim($word));
    $letters = str_split($word);
    $used = [];

    foreach($letters as $letter) {
        if(in_array($letter, $used)) {
            return false;
        }
        preg_match('/[a-z]/', $letter, $matches);
        if(!empty($matches)) {
            $used[] = $letter;
        }
    }

    return true;
}
